terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "5.5.0"
    }
  }
}

provider "google" {
  project     = "gitlab-prep-404401"
  region      = "us-west2"
}

